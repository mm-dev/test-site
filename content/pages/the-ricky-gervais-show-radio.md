+++
title = "The Ricky Gervais Show (Radio)"
shows = ["The Ricky Gervais Show (Radio)"]

writers = [
  "Stephen Merchant",
  "Ricky Gervais",
  "Karl Pilkington"
]
actors = [
  "Stephen Merchant",
  "Ricky Gervais",
  "Karl Pilkington"
]
+++


AKA _XFM_, _The XFM Shows_... whatever you call them, they are among the best bits of media, of any kind, ever created.

This is where [Ricky](/pages/ricky-gervais) and [Steve](/pages/stephen-merchant) first met [Karl](/pages/karl-pilkington) (he was originally just some muggins producer who got lumbered with twiddlin' the knobs of a Saturday).

This is the source for many later shows from the boys. It is well known that when looking for ideas for pretty much all of his future work, Gervais just played the XFM tapes. Jokes, characters and general vibes from these Saturday afternoon radio broadcasts feature liberally in all of the following:

* [The Ricky Gervais Show](/pages/the-ricky-gervais-show) (the animated series)
* The famous podcasts
* [Extras](/pages/extras)
* Various standup routines
