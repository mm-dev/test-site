+++
title = "Derek"
shows = ["Derek"]

producers = ["Charlie Hanson"]
writers = ["Ricky Gervais"]
directors = ["Ricky Gervais"]
actors = [
  "Ricky Gervais",
  "Karl Pilkington"
]
+++


Derek is a lovely sitcom.
