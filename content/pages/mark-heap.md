+++
title = "Mark Heap"
actors = ["Mark Heap"]
writers = ["Mark Heap"]

shows = [
  "Spaced",
  "Green Wing",
  "Look Around You",
  "Big Train",
  "Brass Eye",
  "Jam",
  "Upstart Crow"
]
+++


A stunningly-skilled actor, Heap has brought perfection to many roles in some of the most popular comedy series, but remains, for too many people, one of those "oh it's _him_!" actors whose name they can't be bothered to find out and remember.
