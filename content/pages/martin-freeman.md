+++
title = "Martin Freeman"
actors = ["Martin Freeman"]

shows = [
  "The Office"
]
+++


His portrayal of Tim in [The Office](/pages/the-office) must be one of the most relatable characters anywhere, ever.

If you've not seen it, check out _First responder_ to see Freeman playing a rough scouser and doing a grand job of it.
