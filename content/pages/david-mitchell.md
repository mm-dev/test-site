+++
title = "David Mitchell"
writers = ["David Mitchell"]
actors = ["David Mitchell"]

shows = [
  "Peep Show",
  "That Mitchell and Webb Look",
  "That Mitchell and Webb Situation",
  "That Mitchell and Webb Sound",
  "Back",
  "Upstart Crow"
]
+++


Dry, pedantic and constantly annoyed, David Mitchell always plays the same character --- even, apparently, in real life (judging from his appearances on panel shows and in interviews). He does it so well though, and somehow does manage to stay likeable.
