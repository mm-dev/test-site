+++
title = "Matthew Holness"
writers = ["Matthew Holness"]
actors = ["Matthew Holness"]

shows = [
  "The Office",
  "Back",
  "Garth Merenghi's Darkplace"
]
+++


> Fish-white flesh puckered by the highland breeze. Tight eyes peering out. Screechy booze-soaked voices hollering for a taxi to take 'em to the next pub.
  
> A shatter of glass. A round of applause. A 16-year-old mother of three vomiting in a sewer, bairns looking on, chewing on potato cakes.
  
> I ain't never goin' back. Not never.

Matthew Holness is Garth Merenghi. He also plays (spectacularly) Tim's geek nemesis in [The Office](/pages/the-office).
