+++
title = "Garth Merenghi's Darkplace"
shows = ["Garth Merenghi's Darkplace"]

producers = ["Charlie Hanson"]
writers = [
  "Richard Ayoade",
  "Matthew Holness"
]
actors = [
  "Richard Ayoade",
  "Matthew Holness",
  "Matt Berry",
  "Julian Barratt",
  "Noel Fielding",
  "Graham Linehan",
  "Alice Lowe",
  "Stephen Merchant"
]
directors = [
  "Richard Ayoade"
]
+++


Garth Merenghi's Darkplace is a dark, freakish sitcom.

[Stephen Merchant](/pages/stephen-merchant) makes a disconcerting appearance as a big weird chef.
