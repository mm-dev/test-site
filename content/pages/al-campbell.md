+++
title = "Al Campbell"
directors = ["Al Campbell"]

shows = [
  "Newswipe",
  "How TV Ruined Your Life",
  "Man Down",
  "Screenwipe",
  "Dead Pixels"
]
+++


Al Campbell has directed some great comedy.
