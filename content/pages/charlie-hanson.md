+++
title = "Charlie Hanson"
producers = ["Charlie Hanson"]

shows = [
  "After Life",
  "Derek",
  "Man Down",
  "Extras",
  "Garth Merenghi's Darkplace"
]
+++


Charlie Hanson has produced some great comedy.
